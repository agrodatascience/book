--- 
title: "Science des données pour l’agronomie et l’agroécologie : notes de cours"
author: "Bénédicte Fontez, Meïli Baragatti & Léo Garcia - Montpellier SupAgro"
date: "`r Sys.Date()`"

bibliography: [book.bib,packages.bib]
biblio-style: apalike
link-citations: true
always_allow_html: true

url: http\://agrodatascience.pages.mia.inra.fr/book

description: "Ce document comprend la compilation des notes de cours pour la régression linéaire généralisée et la régression non linéaire. Il a été réalisé grace à **Rmarkdown** et le package **bookdown::gitbook**. Les données et interprétations ont été fournies par Hélène Marroux et Léo Garcia."

site: bookdown::bookdown_site
documentclass: book
output:
  bookdown::gitbook:
    config:
      toc:
        after: |
          <li><a href="http://agrodatascience.pages.mia.inra.fr/book">
          See the source</a></li>
      edit: http://agrodatascience.pages.mia.inra.fr/book/edit/main/%s
      download: "pdf"
  bookdown::pdf_book: 
    latex_engine: xelatex
    citation_package: natbib
---

# Welcome

```{r setup, include=FALSE}
library(magrittr)
```


Ce document comprend la compilation des notes de cours pour la régression linéaire généralisée et la régression non linéaire. Il a été réalisé grace à **Rmarkdown** et le package **bookdown::gitbook**. 

Les données et interprétations ont été fournies par Hélène Marroux et Léo Garcia.

Les packages R @R-base suivants ont été utilisés pour créer ce livre:

* knitr: @xie2015, @R-knitr
* bookdown: @R-bookdown
* rmarkdown: @R-rmarkdown
